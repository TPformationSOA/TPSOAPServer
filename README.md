# Rappel
Vous disposez d'un service web SOAP écrit en Java avec le framework CXF
Ce service a été généré à partir de la WSDL GestionInscriptions-2.0.wsdl
Vous avez réalisé une implémentation, sinon utiliser ce projet GIT TPSOAPServer 
Ici, la localisation du service (url de publication) a été configurée dans application.yaml, 
consulter la manière dont elle est récupérée dans le code.

Testez qu'au moins les 2 opérations inscrireEtudiant et listerInscrits fonctionnent bien en utilisant SOAP-UI


# Exercice 1 : Ajout de la gestion des logs de CXF

* Rajouter la dépendance cxf-rt-features-logging

* Rajouter l'annotation suivante dans votre implémentation

    `@Features(features = "org.apache.cxf.ext.logging.LoggingFeature") `

* Consulter le fichier logback.xml (dans src/main/resources)

Il s'agit du fichier de configuration des logs

* Invoquer le service depuis SOAP-UI

* Lancer plusieurs requêtes, consulter les logs dans les fichiers 

Vérifier que les messages sont bien tracés

Vérifier que les fichiers de logs sont bien rotatifs

# Exercice 2 : Ajout de la validation des messages
* Rajouter l'annotation de validation dans votre implémentation

    `@SchemaValidation(type = SchemaValidation.SchemaValidationType.BOTH)`

* Effectuer des appels avec données valides et données invalides et constater les résultats obtenus

* Modifier l'implémentation pour renvoyer des données invalides et constater de nouveau

* Modifier le type de validation (IN, OUT, NONE, REQUEST, RESPONSE)

* Effectuer de nouveau les tests précédents (différences ente IN/OUT et REQUEST/RESPONSE ?)


