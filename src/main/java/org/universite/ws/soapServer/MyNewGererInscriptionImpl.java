package org.universite.ws.soapServer;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import org.universite.interfaces.etudiant.v1.model.EtudiantType;
import org.universite.interfaces.gestioninscriptions.v2.ErreurTechniqueFault;
import org.universite.interfaces.gestioninscriptions.v2.EtudiantInexistant;
import org.universite.interfaces.gestioninscriptions.v2.EtudiantInexistantFault;
import org.universite.interfaces.gestioninscriptions.v3.*;

/**
 * Implémentation obtenue après copie de l'implémentation de base générée par CXF
 * option -impl de wsdl2java
 * 
 */

@javax.jws.WebService(
                      serviceName = "GererInscriptionSoapService",
                      portName = "GererInscription",
                      targetNamespace = "http://www.universite.org/Interfaces/GestionInscriptions/v3",
                      wsdlLocation = "file:/C:/Users/paki6340/MesFormations/CNAM/TPSOAPServer/src/main/resources/GestionInscriptions-3.0.wsdl",
                      endpointInterface = "org.universite.interfaces.gestioninscriptions.v3.GererInscriptionSoap")

public class MyNewGererInscriptionImpl extends MyGererInscriptionImpl implements GererInscriptionSoap {

    private static final Logger LOG = Logger.getLogger(MyNewGererInscriptionImpl.class.getName());
    

    @Override
    public List<EtudiantType> listerInscrits(String id) throws ErreurTechniqueFault, EtudiantInexistantFault {
        if (null==id || id.isEmpty()) {
            return super.listerInscrits();
        } else {
               if (!myEtudiants.containsKey(id)) {
                                   EtudiantInexistant etudInex=new EtudiantInexistant();
                etudInex.setClasse(this.getClass().getName());
                etudInex.setDescription(ETUDIANT_NOT_FOUND);
                etudInex.setMethode(Thread.currentThread().getStackTrace()[1].getMethodName());
                etudInex.setRaison(ETUDIANT_NOT_FOUND);
                throw new EtudiantInexistantFault(ETUDIANT_NOT_FOUND,etudInex);
               } else {
                    //  ArrayList newListe=new ArrayList<EtudiantType>();
                    //  newListe.add
                     return new ArrayList<EtudiantType>() {
                        {
                          this.add(myEtudiants.get(id));
                        }
                    };
               }
        }
    }
}
