package org.universite.ws.soapServer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.apache.cxf.annotations.SchemaValidation;
import org.apache.cxf.feature.Features;
import org.universite.interfaces.etudiant.v1.model.EtudiantType;
import org.universite.interfaces.gestioninscriptions.v2.*;

/**
 * Impl�mentation obtenue apr�s copie de l'impl�mentation de base g�n�r�e par CXF
 * option -impl de wsdl2java
 * 
 */

@javax.jws.WebService(
                      serviceName = "GererInscriptionSoapService",
                      portName = "GererInscription",
                      targetNamespace = "http://www.universite.org/Interfaces/GestionInscriptions/v2",
                      wsdlLocation = "file:/C:/Users/paki6340/MesFormations/NetBeansProjects/TP/SOAPServer/src/main/resources/GestionInscriptions-2.0.wsdl",
                      endpointInterface = "org.universite.interfaces.gestioninscriptions.v2.GererInscriptionSoap")

public class MyGererInscriptionImpl implements GererInscriptionSoap {
    
    protected static final String ETUDIANT_NOT_FOUND = "Aucun �tudiant correspondant";
    protected static final String EUDIANT_NAME_MULTIPLE = "Plusieurs �tudiants avec ce nom";
    protected static final String NI_ID_NI_NOM = "Impossible de supprimer, param�tres d'entr�e absents";
    
    protected  static Map<String,EtudiantType> myEtudiants=new HashMap<>();

    private static final Logger LOG = Logger.getLogger(MyGererInscriptionImpl.class.getName());


    public void annulerInscription(java.lang.String id, java.lang.String nom) throws ErreurTechniqueFault,  EtudiantInexistantFault   { 
        LOG.info("Executing operation annulerInscription");
        String msg="";
        if (null!=id) {
            msg=supprimeById(id);
        } else {
            msg=supprimeByNom(nom);
        }
        
        if (msg!="") {
               EtudiantInexistant etudInex=new EtudiantInexistant();
                etudInex.setClasse(this.getClass().getName());
                etudInex.setDescription(msg);
                etudInex.setMethode(Thread.currentThread().getStackTrace()[1].getMethodName());
                etudInex.setRaison(msg);
                throw new EtudiantInexistantFault(msg,etudInex);
        }
     }


    public void inscrireEtudiant(org.universite.interfaces.etudiant.v1.model.EtudiantType etudiant) throws InfosEtudiantIncorrectFault,  ErreurTechniqueFault   { 
        LOG.info("Executing operation inscrireEtudiant");
        System.out.println(etudiant);
        try {
            myEtudiants.put(etudiant.getIdentifiant(), etudiant);
        } catch (java.lang.Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        //throw new InfosEtudiantIncorrectFault("InfosEtudiantIncorrectFault...");
        //throw new ErreurTechniqueFault("erreurTechniqueFault...");
    }


    public java.util.List<org.universite.interfaces.etudiant.v1.model.EtudiantType> listerInscrits() throws ErreurTechniqueFault   { 
        LOG.info("Executing operation listerInscrits");
        try {
            return  (List<EtudiantType>) myEtudiants.values().stream().collect(Collectors.toList());
        } catch (java.lang.Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        //throw new ErreurTechniqueFault("erreurTechniqueFault...");
    }
    
    
    /* Suppression via id */
    private String supprimeById(String id) {
        String msg=ETUDIANT_NOT_FOUND;
        if (null!=id && null!=myEtudiants.get(id)) {
            myEtudiants.remove(id);
            msg="";
        }
        return msg;
    }
 
        /* Suppression via id */
    private String supprimeByNom(String nom) {
        String msg=ETUDIANT_NOT_FOUND;
        long trouve=0;
         if (null != nom&&nom.length() > 0) {
             trouve=myEtudiants.values().stream().filter(etudiant -> ((nom.toLowerCase()).equals((etudiant.getNom().toLowerCase())))).count();
              if (trouve==1) {
                 myEtudiants.remove(myEtudiants.values().stream().filter((etudiant) -> ((nom.toLowerCase()).equals((etudiant.getNom().toLowerCase())))).findFirst().get().getIdentifiant());
                     msg="";
             } else {
                   if (trouve > 1) {
                         msg=EUDIANT_NAME_MULTIPLE+" : "+trouve;
                    }
             }
         }
         return msg;
    }
 
}
