package org.universite.ws.soapServer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ServletComponentScan
public class GestionInscriptionApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestionInscriptionApplication.class, args);
	}
}
