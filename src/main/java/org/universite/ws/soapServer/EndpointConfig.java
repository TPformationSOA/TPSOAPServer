/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.universite.ws.soapServer;

/**
 *
 * @author PAKI6340
 */

import javax.xml.ws.Endpoint;
import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;



@Configuration
public class EndpointConfig {

   @Autowired
   private Bus cxfBus;

   @Value("${location}")
   private String location;
   
   @Value("${locationV3}")
   private String locationV3;

  @Bean
   public Endpoint endpoint() {
    EndpointImpl endpoint =
        new EndpointImpl(cxfBus, new MyGererInscriptionImpl());
    endpoint.publish(location);

    return endpoint;
  }

  @Bean
   public Endpoint endpoint2() {
    EndpointImpl endpoint =
        new EndpointImpl(cxfBus, new MyNewGererInscriptionImpl());
    endpoint.publish(locationV3);

    return endpoint;
  }   
   
   
}

